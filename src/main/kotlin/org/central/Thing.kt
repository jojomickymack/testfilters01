package org.central

class Thing(value: String) {
    private val title = value
    init { println("created thing") }
    fun getThing(): String = this.title
}