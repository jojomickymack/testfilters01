package pack2

import org.central.Thing
import org.junit.Assert.assertEquals
import org.junit.Test

class TestGroup3 {

    val thing = Thing("tg3")

    @Test
    fun coolTest1() {
        println("doing sixth test")
        assertEquals(15, 9 + 6)
    }

    @Test
    fun coolTest2() {
        println("doing seventh test")
        assertEquals(16, 9 + 7)
    }

    @Test
    fun coolTest3() {
        println("doing eighth test")
        assertEquals(17, 9 + 8)
    }
}