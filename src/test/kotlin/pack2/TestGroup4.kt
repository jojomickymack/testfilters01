package pack2

import org.central.Thing
import org.junit.Assert.assertEquals
import org.junit.Test

class TestGroup4 {

    val thing = Thing("tg4")

    @Test
    fun coolTest1() {
        println("doing ninth test")
        assertEquals(18, 9 + 9)
    }

    @Test
    fun coolTest2() {
        println("doing tenth test")
        assertEquals(19, 9 + 10)
    }

    @Test
    fun coolTest3() {
        println("doing eleventh test")
        assertEquals(20, 9 + 11)
    }
}