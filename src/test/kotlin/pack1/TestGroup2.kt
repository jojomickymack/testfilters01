package pack1

import org.central.Thing
import org.junit.Assert.assertEquals
import org.junit.Test

class TestGroup2 {

    val thing = Thing("tg2")

    @Test
    fun coolTest4() {
        println("doing fourth test")
        assertEquals(14, 9 + 5)
    }

    @Test
    fun coolTest5() {
        println("doing fifth test")
        assertEquals(15, 9 + 6)
    }

    @Test
    fun coolTest6() {
        println("doing sixth test")
        assertEquals(15, 9 + 6)
    }
}