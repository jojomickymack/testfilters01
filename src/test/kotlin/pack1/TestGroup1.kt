package pack1

import org.central.Thing
import org.junit.Assert.assertEquals
import org.junit.Test

class TestGroup1 {

    val thing = Thing("tg1")

    @Test
    fun coolTest1() {
        println("doing first test")
        assertEquals(14, 9 + 5)
    }

    @Test
    fun coolTest2() {
        println("doing second test")
        assertEquals(15, 9 + 6)
    }

    @Test
    fun coolTest3() {
        println("doing third test")
        assertEquals(15, 9 + 6)
    }
}