# Reasoning About Task Ordering And Test Reports

There are two test tasks which each have a filter (they run all tests which fit the filter). task #1 depends on task #2. The default behavior when running either 1 test that depends on another, or running all tests via the built-in 'tests' task has problems.

- if only task #2 is run, task #2 will get run first but won't be included in the test results, i.e. if task #1 is run, task #2 is run once, task #1 is run once, only task #1's tests are in the report
- if the 'test' task is run (which runs everything with type test, both tasks test results are shown in the report, but task #1 is run before task #2

Problem

 - gradle does not honor 'dependsOn' or 'mustRunAfter' for the default 'test' task, and when one test task depends on another, you only get the test report for the first one

Solution

 - the ['testReport' task](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.TestReport.html) is where you can override this unwanted default behavior.

Also 
 
 - [This gradle plugin](https://github.com/radarsh/gradle-test-logger-plugin) really improves how the test results are displayed when run from the console.

